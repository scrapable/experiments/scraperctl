#!/usr/bin/python3
import argparse
import importlib

parser = argparse.ArgumentParser("scraperctl")
subparsers = parser.add_subparsers()

def register_subcommand(subcommand_name, module_name=None):
    if module_name is None:
        module_name = subcommand_name
    subcommand_mod = importlib.import_module(f"subcommands.{module_name}")
    subparser = subparsers.add_parser(subcommand_name)
    subcommand_mod.configure_parser(subparser)

register_subcommand("status")

args = parser.parse_args()
if hasattr(args, "func"):
    args.func(args)
else:
    parser.print_help()
